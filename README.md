 ![snail-camunda](src/main/resources/logo/snail-camunda.png)
**当前版本：V2.0**
### ⭐项目介绍
本项目是对**Camunda**接口的二次封装以及使用介绍，之所以前缀是**snail【蜗牛】**,是因为做的很慢。<br/> 
本来准备做个前后端分离的流程管理平台，工作太忙，前端知识也忘的差不多了，于是放弃了。😞<br/>
缺点：只有后端代码，不能直观地展示功能。<br/>
**优点：**<br/>
1、不用拉前端代码；  
2、没有其他无关代码，全是干货；  
3、搭配文档或注释，可快速实现你想实现的功能。 
### ⭐项目结构  
```
snail
  |--common            通用模块
     |--base           基本通用功能
     |--exception      异常定义与处理
  |--constant          常量和枚举
  |--controller        控制层
     |--business       业务相关
     |--flow           流程相关
  |--delegate          委托功能
  |--domain            对象模块
     |--entity         实体类
     |--request        接口请求参数类
     |--vo             返回对象
  |--generator         生成器
  |--listener          监听器
  |--mapper            数据持久层
  |--service           相关服务
  |--util              工具包  
```
### ⭐温馨提示
【1.bpmn】是最简单的流程定义，每个节点上只能设置一个审批人🌽  
【2.bpmn】审批人节点上允许设置多个审批人，同个节点上审批人并行🥕  
【3.bpmn】审批人节点上允许设置多个审批人，同个节点上审批人串行🍅  
【4.bpmn】多实例的会签、或签、比例签🍋  
【5.bpmn】Send Task发送邮件🍉  
【6.bpmn】执行监听器与任务监听器事件触发时机演示🥬  
【7.bpmn】排它网关🍡  
【8.bpmn】并行网关🍭  
【9.bpmn】候选人🌯  
【10.bpmn】排它网关🍾  
【11.bpmn】并行网关🍰  
【12.bpmn】依次审批加签🥭
### ⭐使用手册
这里以【10.bpmn】演示如何测试使用：  
1、首先部署流程定义  
![部署](src/main/resources/image/1.png)  
2、查看流程定义，注意这个id后续发起流程实例时需要使用，其他参数在发起流程实例时直接带给流程引擎  
![获取参数](src/main/resources/image/2.png)  
3、发起流程实例，注意这里是直接设置审批人的，通过监听器获取审批人的不要在这里设置，可查看[listener]包下两种监听器使用天数给到4天，预期流程会走到【部长】节点  
![发起流程实例](src/main/resources/image/3.png)  
![发起流程实例](src/main/resources/image/4.png)  
4、查询待办任务，如有需要可在发起流程实例时直接将发起人的待办任务完成。  
设置发起人节点是为了可以实现【发起人撤回】、【驳回到发起人】等功能  
![查询待办任务](src/main/resources/image/5.png)  
5、完成待办任务  
![完成待办任务](src/main/resources/image/6.png)  
6、ip:port 进入Camunda Cockpit查看流程实例走向，是符合我们预期的即可  
![完成待办任务](src/main/resources/image/7.png)  
### ⭐版本计划
**V1.0功能概述**<br/>
🚀发布流程定义<br/>
🚀以字节流访问已发布流程定义<br/>
🚀发起流程实例<br/>
🚀查询待办任务<br/>
🚀完成待办任务<br/>
🚀转办待办任务<br/>
🚀委托待办任务<br/>
🚀查询已办任务<br/>
🚀流程实例的驳回<br/>
🚀流程实例参数问题<br/>   

___

**V1.5预计新增**<br/>
🍄流程实例的挂起与激活<br/>
🍄详解修改流程实例<br/>
🍄查询流程实例历史节点<br/>
🍄会签、或签、比例签<br/>
🍄加签、减签问题<br/>
🍄使用Send Task发送邮件<br/>
🍄任务监听器【Task Listener】<br/>
🍄执行监听器【Execution Listener】<br/>

___

**V2.0预计新增**<br/>
📌排它网关<br/>
📌并行网关<br/>
📌串行加签思路<br/>
📌外部任务【External Tasks】<br/>
📌更多场景探索...<br/>
📌json转bpmn示例图如下：<br/>
![示例图](src/main/resources/image/8.png)   

___  

**后续工作安排**<br/>
💗先前在C站写过几篇文章，但是比较乱，有朋友在Issues提了出文档教学，后续准备按照项目中的案例顺序写一下系列文章。  

### ⭐声明
本项目中相关功能的解决思路并非最佳，如有更好的解决思路可以相关讨论，感谢！
