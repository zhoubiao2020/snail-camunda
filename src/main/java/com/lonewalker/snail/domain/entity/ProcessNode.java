package com.lonewalker.snail.domain.entity;

import lombok.Data;

import java.util.List;

/**
 * @author LoneWalker
 **/
@Data
public class ProcessNode {
    /**
     * 节点id
     */
    private String id;
    /**
     * 节点类型 {@link com.lonewalker.snail.constant.flow.ProcessNodeTypeEnum}
     */
    private Integer type;
    /**
     * 标题
     */
    private String title;
    /**
     * 审批方式 {@link com.lonewalker.snail.constant.flow.ApprovalModeEnum}
     */
    private Integer approvalMode;
    /**
     * 条件表达式
     */
    private String conditionExpression;
    /**
     * 父节点id
     */
    List<String> pIds;

    public List<ProcessNode> parents;
    public List<ProcessNode> children;
}
