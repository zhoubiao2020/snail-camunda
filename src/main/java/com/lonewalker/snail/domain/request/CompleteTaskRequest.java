package com.lonewalker.snail.domain.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author lonewalker
 */
@Data
public class CompleteTaskRequest {

    @NotBlank(message = "流程实例id不能为空")
    private String processInstanceId;

    @NotBlank(message = "任务id不能为空")
    private String taskId;

}
