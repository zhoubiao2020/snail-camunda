package com.lonewalker.snail.domain.request;

import lombok.Data;

/**
 * @description: 部署流程定义的参数类
 * @author: LoneWalker
 * @create: 2022-12-23
 **/
@Data
public class DeployRequestParam {
    /**
     * 名称
     */
    private String bpmnName;

    /**
     * 需要部署的bpmn路径
     */
    private String resourcePath;
}
