package com.lonewalker.snail.domain.request;

import lombok.Data;


/**
 * @author lonewalker
 */
@Data
public class StartProcessRequest {

    /**
     * 流程定义key
     */
    private String processDefinitionKey;

    /**
     * 流程定义id
     */
    private String processDefinitionId;
    /**
     * 业务key
     */
    private String businessKey;
    /**
     * 发起人
     */
    private String initiator;
    /**
     * 租户id
     */
    private String tenantId;
}
