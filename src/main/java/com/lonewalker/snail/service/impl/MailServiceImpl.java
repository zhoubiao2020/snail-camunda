package com.lonewalker.snail.service.impl;

import com.lonewalker.snail.constant.ServiceExceptionEnum;
import com.lonewalker.snail.common.exception.MailException;
import com.lonewalker.snail.service.MailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

/**
 * 邮件服务实现类
 * @author lonewalker
 */
@RequiredArgsConstructor
@Slf4j
@Service
public class MailServiceImpl implements MailService {

    private final JavaMailSender mailSender;
    @Value("${spring.mail.from}")
    private String addresser;

    @Override
    public void sendSimpleMail(String to, String subject, String content) {
        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            //设置发件人
            mailMessage.setFrom(addresser);
            //设置收件人
            mailMessage.setTo(to);
            //设置主题
            mailMessage.setSubject(subject);
            //设置内容
            mailMessage.setText(content);
            //发送邮件
            mailSender.send(mailMessage);
            log.info("邮件发送成功");
        } catch (org.springframework.mail.MailException e) {
            throw new MailException(ServiceExceptionEnum.SEND_MAIL_EXCEPTION.getCode(),ServiceExceptionEnum.SEND_MAIL_EXCEPTION.getDesc());
        }
    }

    @Override
    public void sendHtmlMail(String to, String subject, String content) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);
            //设置发送者
            messageHelper.setFrom(addresser);
            //设置收件人
            messageHelper.setTo(to);
            //设置主题
            messageHelper.setSubject(subject);
            //设置内容
            messageHelper.setText(content,true);
            mailSender.send(message);
            log.info("邮件发送成功");

        } catch (MessagingException e) {
            throw new MailException(ServiceExceptionEnum.SEND_MAIL_EXCEPTION.getCode(),ServiceExceptionEnum.SEND_MAIL_EXCEPTION.getDesc());
        }
    }

    @Override
    public void sendAttachmentsMail(String to, String subject, String content, String filePath) {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(addresser);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content, true);

            FileSystemResource file = new FileSystemResource(new File(filePath));
            String fileName = filePath.substring(filePath.lastIndexOf(File.separator));
            helper.addAttachment(fileName, file);
            mailSender.send(message);
            log.info("邮件发送成功");
        } catch (MessagingException e) {
            throw new MailException(ServiceExceptionEnum.SEND_MAIL_EXCEPTION.getCode(),ServiceExceptionEnum.SEND_MAIL_EXCEPTION.getDesc());
        }
    }
}
