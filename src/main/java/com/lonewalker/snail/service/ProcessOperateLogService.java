package com.lonewalker.snail.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lonewalker.snail.domain.entity.ProcessOperateLog;

/**
 * @author lonewalker
 */
public interface ProcessOperateLogService extends IService<ProcessOperateLog> {

}
