package com.lonewalker.snail.service;

import com.lonewalker.snail.common.base.ApiResult;
import com.lonewalker.snail.domain.entity.SysUser;
import com.lonewalker.snail.domain.request.*;
import com.lonewalker.snail.domain.vo.TaskVo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * @author lonewalker
 */
public interface ProcessTaskService {


    /**
     * 查询待办任务--针对单个业务
     *
     * @param userInfo    用户信息
     * @param businessKey 业务Key
     * @return 任务相关信息
     */
    ApiResult<List<TaskVo>> listToDoTaskBySingle(SysUser userInfo, String businessKey);

    /**
     * 查询待办任务--针对多个业务
     *
     * @param userInfo     用户信息
     * @param businessKeys 业务Key
     * @return 任务相关信息
     */
    ApiResult<List<TaskVo>> listToDoTaskByMulti(SysUser userInfo, String[] businessKeys);

    /**
     * 分页查询待办任务
     *
     * @param userInfo    用户信息
     * @param businessKey 业务Key
     * @param pageNum     页码
     * @param pageSize    页数
     * @return 任务相关信息
     */
    ApiResult<List<TaskVo>> pageToDoTask(SysUser userInfo, String businessKey, Integer pageNum, Integer pageSize);

    /**
     * 完成单个任务
     *
     * @param requestParam 请求参数
     * @return 任务所在节点信息
     */
    ApiResult<Map<String, String>> completeSingleTask(CompleteTaskRequest requestParam);

    /**
     * 转交待办任务
     *
     * @param requestParam 请求参数
     * @return Void
     */
    ApiResult<Void> transferTask(TransferTaskRequest requestParam);

    /**
     * 委托待办任务
     * 将任务委托给另一个用户，新的受让人必须使用resolveTask(String)向所有者报告。只有所有者才能完成任务。
     * Assignee此时为受让人，Owner是原用户
     *
     * @param requestParam 请求参数
     * @return Void
     */
    ApiResult<Void> delegateTask(DelegateTaskRequest requestParam);

    /**
     * 解决任务
     * 标志受让人已完成委托给他的任务，并且可以将其发送回所有者
     *
     * @param requestParam 请求参数
     * @return Void
     */
    ApiResult<Void> resolveTask(ResolveTaskRequest requestParam);

    /**
     * 查询已办任务
     *
     * @param userInfo    用户信息
     * @param businessKey 业务key
     * @return 已办任务
     */
    ApiResult<List<TaskVo>> listDoneTask(SysUser userInfo, String businessKey);

    /**
     * 添加任务分配人
     *
     * @param requestParam 请求参数
     * @return 提示信息
     */
    ApiResult<String> addAssignee(AddAssigneeRequest requestParam);

    /**
     * 查询候选任务
     * @param userInfo 用户信息
     * @return 任务id
     */
    ApiResult<List<TaskVo>> listCandidateTask(SysUser userInfo);

    /**
     * 拾取任务
     * @param userInfo 用户信息
     * @param taskId   任务id
     * @return 提示信息
     */
    ApiResult<String> claimTask(SysUser userInfo,String taskId);

    /**
     * 归还任务
     * @param userInfo 用户信息
     * @param taskId   任务id
     * @return 提示信息
     */
    ApiResult<String> giveBackTask(SysUser userInfo,String taskId);
}
