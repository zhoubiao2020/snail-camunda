package com.lonewalker.snail.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lonewalker.snail.domain.entity.ProcessOperateLog;

/**
 * @author LoneWalker
 */
public interface ProcessOperateLogMapper extends BaseMapper<ProcessOperateLog> {
}
