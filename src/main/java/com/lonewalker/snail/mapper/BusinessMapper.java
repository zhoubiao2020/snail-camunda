package com.lonewalker.snail.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lonewalker.snail.domain.entity.Business;

/**
 * @author LoneWalker
 */
public interface BusinessMapper extends BaseMapper<Business> {
}
