package com.lonewalker.snail.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lonewalker.snail.domain.entity.ProcessInstance;

/**
 * @author LoneWalker
 */
public interface ProcessInstanceMapper extends BaseMapper<ProcessInstance> {
}
