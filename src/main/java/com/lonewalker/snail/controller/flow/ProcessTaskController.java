package com.lonewalker.snail.controller.flow;

import com.lonewalker.snail.common.base.ApiResult;
import com.lonewalker.snail.domain.entity.SysUser;
import com.lonewalker.snail.domain.request.AddAssigneeRequest;
import com.lonewalker.snail.domain.vo.TaskVo;
import com.lonewalker.snail.service.ProcessTaskService;
import com.lonewalker.snail.util.UserUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 主要演示加签、减签
 *
 * @author lonewalker
 */
@RequestMapping("/process/task")
@RequiredArgsConstructor
@RestController
public class ProcessTaskController {

    private final ProcessTaskService processTaskService;

    /**
     * 添加任务分配人[加签]
     *
     * @param requestParam 请求参数
     * @return 提示信息
     */
    @PostMapping("/add")
    public ApiResult<String> addAssignee(@RequestBody @Validated AddAssigneeRequest requestParam) {
        return processTaskService.addAssignee(requestParam);
    }

    /**
     * 查询候选任务
     * @return 任务
     */
    @GetMapping("/listCandidateTask")
    public ApiResult<List<TaskVo>> listCandidateTask(){
        //模拟获取登录用户信息
        SysUser userInfo = UserUtil.getUserInfo();
        return processTaskService.listCandidateTask(userInfo);
    }

    /**
     * 领取任务
     * @param taskId 任务id
     * @return 提示信息
     */
    @GetMapping("/claimTask")
    public ApiResult<String> claimTask(String taskId){
        SysUser userInfo = UserUtil.getUserInfo();
        return processTaskService.claimTask(userInfo,taskId);
    }

    /**
     * 归还任务
     * @param taskId 任务id
     * @return 提示信息
     */
    @GetMapping("/giveBackTask")
    public ApiResult<String> giveBackTask(String taskId){
        SysUser userInfo = UserUtil.getUserInfo();
        return processTaskService.giveBackTask(userInfo,taskId);
    }

}
