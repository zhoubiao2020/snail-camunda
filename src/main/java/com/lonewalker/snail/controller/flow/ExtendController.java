package com.lonewalker.snail.controller.flow;

import com.lonewalker.snail.common.base.ApiResult;
import com.lonewalker.snail.domain.request.SequentialAddRequest;
import com.lonewalker.snail.service.ExtendService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 扩展功能专属
 *
 * @author lonewalker
 */
@RequestMapping("/extend")
@RequiredArgsConstructor
@RestController
public class ExtendController {

    private final ExtendService extendService;

    /**
     * 依次审批加签
     *
     * @param requestParam 请求参数
     * @return String
     */
    @PostMapping("/addAssignee")
    public ApiResult<String> addAssignee(@RequestBody @Validated SequentialAddRequest requestParam){
        return extendService.addAssignee(requestParam);
    }


}
