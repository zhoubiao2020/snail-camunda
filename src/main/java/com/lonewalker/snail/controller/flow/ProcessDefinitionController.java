package com.lonewalker.snail.controller.flow;

import com.lonewalker.snail.common.base.ApiResult;
import com.lonewalker.snail.domain.entity.SysUser;
import com.lonewalker.snail.domain.request.CreateDefinitionRequest;
import com.lonewalker.snail.domain.request.DeployRequestParam;
import com.lonewalker.snail.service.ProcessDefinitionService;
import com.lonewalker.snail.util.UserUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 流程定义相关接口
 * @author lonewalker
 */
@RequestMapping("/process/definition")
@RequiredArgsConstructor
@RestController
public class ProcessDefinitionController {

    private final ProcessDefinitionService processDefinitionService;
    /**
     * 发布流程定义
     * @param requestParam 请求参数
     * @return 提示信息
     */
    @PostMapping("/deploy")
    public ApiResult<String> deployProcessDefinition(@RequestBody DeployRequestParam requestParam){
        SysUser userInfo = UserUtil.getUserInfo();
        return processDefinitionService.deploy(userInfo,requestParam);
    }

    /**
     * 删除部署
     * @param deploymentId 部署id
     * @return 提示信息
     */
    @DeleteMapping("/deleteDeployment")
    public ApiResult<String> deleteDeployment(@RequestParam("deploymentId")String deploymentId){
        return processDefinitionService.deleteDeployment(deploymentId);
    }

    /**
     * 挂起流程定义
     * @param processDefinitionId 流程定义id
     * @return 提示信息
     */
    @PostMapping("/suspendById")
    public ApiResult<String> suspendProcessDefinitionById(@RequestParam("processDefinitionId")String processDefinitionId){
        return processDefinitionService.suspendProcessDefinitionById(processDefinitionId);
    }

    @GetMapping("/getBpmnModelInstance")
    public ApiResult<String> getBpmnModelInstance(@RequestParam("processDefinitionId")String processDefinitionId){
        return processDefinitionService.getBpmnModelInstance(processDefinitionId);
    }

    /**
     * json转为bpmn
     *
     * @param requestParam 请求参数
     * @return
     */
    @PostMapping("/convert")
    public ApiResult<String> convert(@RequestBody @Validated CreateDefinitionRequest requestParam){
        return processDefinitionService.convert(requestParam);
    }
}
