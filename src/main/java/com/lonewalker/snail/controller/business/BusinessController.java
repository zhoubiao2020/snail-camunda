package com.lonewalker.snail.controller.business;

import com.lonewalker.snail.common.base.ApiResult;
import com.lonewalker.snail.domain.entity.SysUser;
import com.lonewalker.snail.domain.request.AddBusinessRequest;
import com.lonewalker.snail.service.BusinessService;
import com.lonewalker.snail.util.UserUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 业务接口
 * @author lonewalker
 */
@RequestMapping("/business")
@RestController
public class BusinessController {

    @Resource
    private BusinessService businessService;

    @PostMapping("/add")
    public ApiResult<Void> add(@RequestBody AddBusinessRequest request){
        SysUser userInfo = UserUtil.getUserInfo();
        return businessService.add(userInfo,request);
    }

    @PostMapping("/update")
    public ApiResult<Void> update(@RequestBody AddBusinessRequest request){
        SysUser userInfo = UserUtil.getUserInfo();
        return businessService.update(userInfo,request);
    }
}
