package com.lonewalker.snail.constant.flow;

/**
 * bpmn常量类
 *
 * @author Lonewalker
 **/
public interface BpmnConstant {
    String PROCESS = "process";
    String PLANE = "plane";
    String DIAGRAM = "diagram";
    String SEQUENCE = "sequence";
    String SEQUENCE_TARGET_POINT = "sequenceTargetPoint";
    /**
     * 空字符串
     */
    String EMPTY_STR = "";
    /**
     * model实例
     */
    String MODEL_INSTANCE = "modelInstance";
    /**
     * 当前节点
     */
    String CURRENT_NODE = "currentNode";
    /**
     * 当前节点的图
     */
    String CURRENT_NODE_BOUND = "currentNodeBound";
    /**
     * 发起人节点id
     */
    String INITIATOR_NODE_ID = "root";
    /**
     * 结束节点
     */
    String END_NODE = "endNode";
    /**
     * 发起节点的审批人参数
     */
    String START_ASSIGNEE = "${initiator}";
    /**
     * 用户任务节点审批人参数
     */
    String USER_TASK_ASSIGNEE = "${assignee}";
    /**
     * 多实例集合参数
     */
    String MULTI_INSTANCE_COLLECTION = "${assigneeList}";
    /**
     * 多实例元素变量
     */
    String ELEMENT_VARIABLE = "assignee";
    /**
     * 完成条件  一个
     */
    String COMPLETION_CONDITION_ONE = "${ nrOfCompletedInstances == 1}";
    /**
     * 完成条件  所有
     */
    String COMPLETION_CONDITION_ALL = "${nrOfInstances == nrOfCompletedInstances}";
    /**
     * 执行监听器
     */
    String EXECUTION_LISTENER = "com.lonewalker.snail.listener.FlowExecutionListener";
    /**
     * 发送任务委托类
     */
    String SEND_TASK_DELEGATE = "com.lonewalker.snail.delegate.SendEmailDelegate";
    /**
     * 子节点个数
     */
    String CHILD_SIZE = "childSize";
    /**
     * bpmn后缀名
     */
    String BPMN_SUFFIX = ".bpmn";
    /**
     * 值对应排他网关
     */
    String EXCLUSIVE_GATEWAY = "exclusiveGateway";

    String EXCLUSIVE_GATEWAY_BOUND = "exclusiveGatewayBound";

    /**
     * multiInstanceBody
     */
    String MULTI_INSTANCE_BODY ="#multiInstanceBody";

    /**
     * 开始节点的X坐标
     */
    Double START_NODE_X = 180.0;
    /**
     * 开始节点的Y坐标
     */
    Double START_NODE_Y = 100.0;
    /**
     * 开始节点的宽
     */
    Double START_BOUND_WIDTH = 36.0;
    /**
     * 开始节点的高
     */
    Double START_BOUND_HEIGHT = 36.0;
    /**
     * 任务节点的宽
     */
    Double TASK_BOUND_WIDTH = 100.0;
    /**
     * 任务节点的高
     */
    Double TASK_BOUND_HEIGHT = 80.0;
    /**
     * 线长
     */
    Double SEQUENCE_LENGTH = 100.0;
    /**
     * 网关的宽
     */
    Double GATEWAY_WIDTH = 50.0;
    /**
     * 网关的高
     */
    Double GATEWAY_HEIGHT = 50.0;
    /**
     * 流程定义Key前缀
     */
    String PROCESS_DEFINITION_KEY_PREFIX = "Process";
    /**
     * 流程定义Key随机位数
     */
    int PROCESS_DEFINITION_KEY_RANDOM = 8;


}
