package com.lonewalker.snail.constant.flow;

/**
 * @description: 流程相关常量类
 * @author: lonewalker
 * @create: 2022-12-26
 **/
public class ProcessConstant {

    public static String ROOT = "root";

    public static String ROOT_NAME = "发起人";
    /**
     * 节点id
     */
    public static String NODE_ID = "nodeId";
    /**
     * 节点名称
     */
    public static String NODE_NAME = "nodeName";

    /**
     * completed
     */
    public static String COMPLETED = "completed";

    /**
     * multiInstanceBody
     */
    public static String MULTI_INSTANCE_BODY = "#multiInstanceBody";
    /**
     * 发起人
     */
    public static String INITIATOR = "initiator";

    /**
     * 空字符串
     */
    public static String EMPTY = "";
}
