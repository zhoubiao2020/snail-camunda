package com.lonewalker.snail.constant.flow;

import lombok.Getter;

/**
 * 流程节点类型枚举
 *
 * @author Lonewalker
 */
@Getter
public enum ProcessNodeTypeEnum {
    /**
     * 发起节点
     */
    INITIATOR(1, "发起节点"),
    /**
     * 审批节点
     */
    APPROVAL(2, "审批节点"),
    /**
     * 分支
     */
    BRANCH(3, "分支条件"),
    /**
     * 抄送
     */
    CARBON_COPY(4, "抄送节点"),
    /**
     * 办理
     */
    HANDLE(5, "办理节点"),
    ;


    /**
     * 编码
     */
    private final Integer code;
    /**
     * 描述
     */
    private final String Description;

    ProcessNodeTypeEnum(Integer code, String description) {
        this.code = code;
        Description = description;
    }
}
