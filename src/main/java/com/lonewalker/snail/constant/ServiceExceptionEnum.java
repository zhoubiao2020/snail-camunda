package com.lonewalker.snail.constant;

import lombok.Getter;

/**
 * @author lonewalker
 */

@Getter
public enum ServiceExceptionEnum {
    /**
     * 服务异常
     */
    SERVE_EXCEPTION(2000,"服务异常，请联系管理员"),
    /**
     * 邮件异常
     */
    SEND_MAIL_EXCEPTION(2001,"发送邮件异常"),

    SUSPENDED_EXCEPTION(3001,"流程实例已被挂起，无法操作")


    ;



    private final Integer code;
    private final String desc;

    ServiceExceptionEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
