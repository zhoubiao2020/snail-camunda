package com.lonewalker.snail.constant;

/**
 * 参数key常量类
 *
 * @author lonewalker
 **/
public interface ParamKeyConstant {
    /**
     * 发起人
     */
    String INITIATOR = "initiator";
    /**
     * 应用标识
     */
    String APP_NAME = "appName";
    /**
     * 租户id
     */
    String TENANT_ID = "tenantId";
    /**
     * 流程实例id
     */
    String PROCESS_INSTANCE_ID = "processInstanceId";
    /**
     * 节点id
     */
    String NODE_ID = "nodeId";
    /**
     * 节点名称
     */
    String NODE_NAME = "nodeName";
    /**
     * 下一节点id
     */
    String NEXT_NODE_ID = "nextNodeId";
    /**
     * 下一节点名称
     */
    String NEXT_NODE_NAME = "nextNodeName";
    /**
     * 结束标识
     */
    String FINISHED_TAG = "finishedTag";
    /**
     * 流程用的审批人参数名
     */
    String ASSIGNEE = "assignee";
    /**
     * 流程用的审批人参数名
     */
    String ASSIGNEE_LIST = "assigneeList";
    /**
     * 业务id
     */
    String BUSINESS_ID = "businessId";
    /**
     * 临时流程实例id
     */
    String TEMPORARY_INSTANCE_ID = "temporaryInstanceId";


}
