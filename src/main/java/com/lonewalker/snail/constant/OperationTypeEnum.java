package com.lonewalker.snail.constant;

import lombok.Getter;

/**
 * @author wangyu
 */

@Getter
public enum OperationTypeEnum {
    /**
     * 发起人撤回
     */
    INITIATOR_REVOCATION(1),

    /**
     * 审批拒绝
     */
    APPROVAL_REJECTION(2),

    /**
     * 审批撤回
     */
    APPROVAL_REVOCATION(3),
    ;


    /**
     * 码值
     */
    private final Integer code;

    OperationTypeEnum(Integer code) {
        this.code = code;
    }
}
