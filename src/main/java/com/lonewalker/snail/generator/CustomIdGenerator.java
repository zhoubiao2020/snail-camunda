package com.lonewalker.snail.generator;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import org.camunda.bpm.engine.impl.cfg.IdGenerator;
import org.springframework.stereotype.Component;

/**
 * @author lonewalker
 */
@Component
public class CustomIdGenerator implements IdGenerator {
    @Override
    public String getNextId() {
        return IdWorker.getIdStr();
    }
}
