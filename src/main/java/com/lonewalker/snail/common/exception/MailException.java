package com.lonewalker.snail.common.exception;


/**
 * 邮件异常
 *
 * @author lonewalker
 */
public class MailException extends BaseException {
    private static final long serialVersionUID = 1L;

    public MailException(Integer code, String message) {
        super("mail",code, message);
    }
}
