package com.lonewalker.snail.common.exception;

import cn.hutool.http.HttpStatus;
import lombok.Getter;

/**
 * 业务异常
 * @author LoneWalker
 */
@Getter
public class ServiceInvokeException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private final Integer code;

    public ServiceInvokeException(String message) {
        super(message);
        code = HttpStatus.HTTP_BAD_REQUEST;
    }

    public ServiceInvokeException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}


