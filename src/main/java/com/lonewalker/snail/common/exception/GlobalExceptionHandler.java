package com.lonewalker.snail.common.exception;

import com.lonewalker.snail.common.base.ApiResult;
import com.lonewalker.snail.constant.ServiceExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.SuspendedEntityInteractionException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @description: 异常处理器
 * @author: lonewalker
 * @create: 2022-12-23
 **/
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NullPointerException.class)
    public ApiResult<String> nullPointerExceptionHandler(NullPointerException exception) {
        log.error("业务异常",exception);
        return ApiResult.failMessage(ServiceExceptionEnum.SERVE_EXCEPTION.getDesc());
    }

    @ExceptionHandler(ServiceInvokeException.class)
    public ApiResult<String> serviceInvokeExceptionHandler(ServiceInvokeException exception){
        log.error("业务异常",exception);
        return ApiResult.failMessage(exception.getMessage());
    }

    @ExceptionHandler(ProcessException.class)
    public ApiResult<String> serviceInvokeExceptionHandler(ProcessException exception){
        log.error("流程异常",exception);
        return ApiResult.failMessage(ServiceExceptionEnum.SERVE_EXCEPTION.getDesc());
    }

    @ExceptionHandler(SuspendedEntityInteractionException.class)
    public ApiResult<String> suspendedExceptionHandler(SuspendedEntityInteractionException exception){
        return ApiResult.failMessage(ServiceExceptionEnum.SUSPENDED_EXCEPTION.getDesc());
    }

    @ExceptionHandler(MailException.class)
    public ApiResult<String> mailExceptionHandler(MailException exception){
        return ApiResult.successMessage(exception.getMessage());
    }
}
