package com.lonewalker.snail.common.exception;

import lombok.Getter;

/**
 * 流程异常
 *
 * @author LoneWalker
 */
@Getter
public class ProcessException extends BaseException {
    private static final long serialVersionUID = 1L;

    public ProcessException(Integer code, String message) {
        super("process", code, message);
    }
}
