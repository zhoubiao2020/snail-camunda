package com.lonewalker.snail.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;

/**
 * 任务监听器
 *
 * @author lonewalker
 */
@RequiredArgsConstructor
@Slf4j
@Component
public class FlowTaskListener implements TaskListener {
    /**
     * 1、在create事件之前不会触发其他与任务相关的事件
     * 2、当分配人、所有者等属性发生变更时会触发update事件，任务的初始化是不会触发update事件的。
     * 3、在流程定义中显式定义了具有受让人的任务被创建时，assignment事件会在create事件之后触发。
     * 更改任务assignee属性，assignment事件会在update事件之后触发。
     * 4、当任务成功完成时，触发complete事件。
     * 5、delete事件发生在任务从运行时数据中删除之前。
     * 6、complete事件和delete事件是互斥的。
     */
    @Override
    public void notify(DelegateTask delegateTask) {
        log.info("任务监听器-----" + delegateTask.getEventName());
    }
}
