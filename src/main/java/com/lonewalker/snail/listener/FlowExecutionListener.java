package com.lonewalker.snail.listener;

import com.lonewalker.snail.constant.ParamKeyConstant;
import com.lonewalker.snail.constant.flow.ProcessConstant;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.ActivityTypes;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.impl.persistence.entity.ExecutionEntity;
import org.camunda.bpm.engine.impl.pvm.process.TransitionImpl;
import org.camunda.bpm.engine.variable.Variables;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 执行监听器
 * @author lonewalker
 */
@RequiredArgsConstructor
@Slf4j
@Component
public class FlowExecutionListener implements ExecutionListener {
    private final RepositoryService repositoryService;
    @Override
    public void notify(DelegateExecution execution) throws Exception {
        log.info("执行监听器-----"+execution.getEventName());
//        ExecutionEntity executionEntity = (ExecutionEntity) execution;
//        TransitionImpl transition = executionEntity.getTransition();
//        //连线的目标节点id
//        String targetNodeId = transition.getDestination().getId();
//
//        if (targetNodeId.contains(ActivityTypes.MULTI_INSTANCE_BODY)) {
//            targetNodeId = targetNodeId.substring(0, targetNodeId.indexOf("#"));
//        }
//
//        //流程定义id
//        String processDefinitionId = executionEntity.getProcessDefinitionId();
//        //流程定义key
//        String processDefinitionKey = processDefinitionId.substring(0, processDefinitionId.indexOf(":"));
//        //获取流程定义的版本
//        final int version = repositoryService.getProcessDefinition(processDefinitionId).getVersion();
//        //流程实例id
//        String processInstanceId = executionEntity.getProcessInstanceId();
//        //发起人
//        String initiator = execution.getVariable(ProcessConstant.INITIATOR).toString();
//
//        //以上的参数是用来查询审批人的，用不上都可以去掉。动态审批人，在设计bpmn时可以将节点审批人参数名定为一样的，比如我这里就都是assigneeList
//
        List<String> assigneeList = new ArrayList<>();
        assigneeList.add("10089");
        assigneeList.add("10090");
        assigneeList.add("10091");
        assigneeList.add("10088");
        execution.setVariable(ParamKeyConstant.ASSIGNEE_LIST, assigneeList);
    }
}
